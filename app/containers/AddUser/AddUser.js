import React, { useState, memo, useEffect } from 'react';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'redux';
import saga from './saga';
import { makeSelecError } from './selectors';
import reducer from './reducers';
import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';
import '../../components/css/style.css';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { useLocation } from 'react-router-dom';
import { addUser } from './actions';
import { makeSelectUser } from '../User/selectors';
import { getUser } from '../User/actions';
import { addError } from './actions';
import { useSnackbar } from 'notistack';
import { setDefault } from './actions';
const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
}));

const User = ({
  users,
  loading,
  error,
  addUser,
  getUsers,
  addError,
  setDefault,
}) => {
  const classes = useStyles();
  const key = 'addUser';
  useInjectSaga({ key, saga });
  useInjectReducer({ key, reducer });
  const history = useHistory();
  const location = useLocation();
  const [status, setStatus] = useState(location.state.status);
  const [userEdit, SetUserEdit] = useState({
    name: ' ',
    phone: ' ',
    address: ' ',
    website: ' ',
    company: ' ',
    email: ' ',
  });
  useEffect(() => {
    if (status == 'Edit') {
      SetUserEdit(location.state.user);
    }
    if (users === undefined) history.push('/');
    setTimeout(() => {
      setDefault();
    }, 1);
  }, []);

  const backHome = () => {
    if (error === null) {
      history.goBack();
    }
  };
  // validation form
  function validateName(text) {
    for (let index = 0; index < users.length; index++) {
      if (users[index].name.trim() == text.trim()) return 101;
    }
  }
  // function validateExistName(text) {
  //   var nameRegex = /^[a-zA-Z0-9]+$/;
  //   console.log(text.match(nameRegex));
  //   // if (text.match(nameRegex) != undefined) {
  //   //   return 102;
  //   // }
  // }
  const validation = () => {
    if (validateName(userEdit.name.trim()) !== undefined) {
      handleClick('Tên này đã tồn tại');
      addError(validateName(userEdit.name.trim()));
    }
    if (userEdit.name.trim() == '') {
      handleClick('Không được để trống ');
      addError(102);
    }
  };

  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const handleClick = context => {
    enqueueSnackbar(context);
  };
  console.log(error);
  return (
    <>
      <div className={classes.root}>
        <Grid
          container
          spacing={4}
          item
          xs={10}
          sm={10}
          style={{ display: 'flex', margin: 'auto' }}
        >
          <Grid item xs={12} sm={12} style={{ textAlign: 'center' }}>
            Add New User
          </Grid>

          <Grid item xs={6} sm={6}>
            <form className={classes.root} noValidate autoComplete="off">
              <TextField
                id="standard-basic"
                fullWidth
                label="Name"
                required
                InputProps={{
                  readOnly: status === 'Edit',
                }}
                value={userEdit.name}
                onChange={e =>
                  SetUserEdit({
                    ...userEdit,
                    name: e.target.value,
                  })
                }
                onBlur={() => validation()}
              />
            </form>
          </Grid>
          <Grid item xs={6} sm={6}>
            <form className={classes.root} noValidate autoComplete="off">
              <TextField
                id="standard-basic"
                fullWidth
                label="Phone"
                value={userEdit.phone}
                onChange={e =>
                  SetUserEdit({
                    ...userEdit,
                    phone: e.target.value,
                  })
                }
                onBlur={() => validation()}
                required
              />
            </form>
          </Grid>
          <Grid item xs={6} sm={6}>
            <form className={classes.root} noValidate autoComplete="off">
              <TextField
                id="standard-basic"
                fullWidth
                label="Address "
                value={userEdit.address}
                onChange={e =>
                  SetUserEdit({
                    ...userEdit,
                    address: e.target.value,
                  })
                }
                onBlur={() => validation()}
                required
              />
            </form>
          </Grid>
          <Grid item xs={6} sm={6}>
            <form className={classes.root} noValidate autoComplete="off">
              <TextField
                id="standard-basic"
                fullWidth
                label="Website "
                value={userEdit.website}
                onChange={e =>
                  SetUserEdit({
                    ...userEdit,
                    website: e.target.value,
                  })
                }
                onBlur={() => validation()}
                required
              />
            </form>
          </Grid>
          <Grid item xs={6} sm={6}>
            <form className={classes.root} noValidate autoComplete="off">
              <TextField
                id="standard-basic"
                fullWidth
                label="Company "
                value={userEdit.company}
                onChange={e =>
                  SetUserEdit({
                    ...userEdit,
                    company: e.target.value,
                  })
                }
                onBlur={() => validation()}
                required
              />
            </form>
          </Grid>
          <Grid item xs={6} sm={6}>
            <form className={classes.root} noValidate autoComplete="off">
              <TextField
                id="standard-basic"
                fullWidth
                label="Email "
                value={userEdit.email}
                onChange={e =>
                  SetUserEdit({
                    ...userEdit,
                    email: e.target.value,
                  })
                }
                onBlur={() => validation()}
                required
              />
            </form>
          </Grid>
          <Grid
            item
            xs={12}
            sm={12}
            style={{ display: 'flex', justifyContent: 'space-between' }}
          >
            <Button
              variant="contained"
              color="primary"
              style={{ marginLeft: '25%' }}
              onClick={() => history.goBack()}
            >
              Cancel
            </Button>
            <Button
              variant="contained"
              color="secondary"
              sx={{ marginRight: '25%' }}
              onClick={() => {
                addUser(status, userEdit);
                backHome();
              }}
            >
              {status}
            </Button>
          </Grid>
        </Grid>
      </div>
    </>
  );
};
User.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  repos: PropTypes.oneOfType([PropTypes.array, PropTypes.bool]),
};
const mapStateToProps = createStructuredSelector({
  // repos: makeSelectRepos(),
  users: makeSelectUser(),
  // loading: makeSelectLoading(),
  error: makeSelecError(),
});

export function mapDispatchToProps(dispatch) {
  return {
    addUser: (status, user) => {
      dispatch(addUser(status, user));
    },
    getUsers: () => {
      dispatch(getUser());
    },

    addError: er => {
      dispatch(addError(er));
    },
    setDefault: () => {
      dispatch(setDefault());
    },
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(User);
