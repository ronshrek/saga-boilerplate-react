import * as type from './types';
const initialState = {
  loading: false,
  error: [],
};
export default function users(state = initialState, action) {
  switch (action.type) {
    case type.GET_ADD_DEFAULT:
      console.log('default');
      return {
        error: [],
        loading: false,
      };
    case type.GET_ADD_REQUEST:
      return {
        error: [],
        loading: true,
      };
    case type.GET_ADD_SUCCESS:
      return {
        error: [],
        loading: false,
      };
    case type.GET_ADD_FAILED:
      return {
        loading: false,
        error: [...action.message],
      };
    case type.SET_ERROR:
      let newError = [action.payload];

      let flag = true;
      for (let index = 0; index < state.error.length; index++) {
        if (state.error[index] == newError) {
          flag = false;
          break;
        }
      }
      if (flag) newError = [...state.error, ...newError];
      return {
        error: [...newError],
        loading: false,
      };
    default:
      return state;
  }
}
